/**
 * @author Wessam Farid
 * 
 * Satellite ground operations for an earth science mission that monitors magnetic field variations 
 * at the Earth's poles. A pair of satellites fly in tandem orbit such that at least one will have line 
 * of sight with a pole to take accurate readings. The satellite�s science instruments are sensitive to 
 * changes in temperature and must be monitored closely. Onboard thermostats take several temperature readings 
 * every minute to ensure that the precision magnetometers do not overheat. Battery systems voltage levels are 
 * also monitored to ensure that power is available to cooling coils. Design a monitoring and alert application 
 * that processes status telemetry from the satellites and generates alert messages in cases of certain limit violation scenarios.
 * 
 * Ingest status telemetry data and create alert messages for the following violation conditions:
 *       	If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
 * 			If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
 */
package com.pagingmissioncontrol;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class PagingMissionControl {
	
	public static int battCounter =0;		// Battary Voltage Alert Counter
	public static String batteryTS = ""; 	// Battary TimeStamp
	public static String batteryId = ""; 	// Satellite ID
	public static int thermoCounter =0;		// Thermostat Alert Counter
	public static String thermoTS = "";		// Thermostat TimeStamp
	public static String thermoId = "";		// Satellite ID
	public static boolean resultEmpty = true;
	
	/*
	 * Param:  fields, localDateTime
	 * Description: This method will process Battary Voltage data and check to see if we have an alert to display
	 * if we have an alert then append it to the output result
	 */
	public static String processBattary(String[] fields, LocalDateTime localDateTime) {
		
		String result="";		
		float rawValue = Float.parseFloat(fields[6]);
		int red_low = Integer.parseInt(fields[5]);
		String component = fields[7];
		if (component.equalsIgnoreCase("BATT") && rawValue < red_low) {
			if (battCounter == 0) {
				batteryTS = localDateTime.toString();
    			batteryId = fields[1];
			}    			
			battCounter++;    				    			
		}	
		// If we reach 3 times within 5 mins then append the alert and reset the counter
		if (battCounter == 3) {
			battCounter = 0;
			// Append comma if result is not empty
			if (resultEmpty) {
				result = result + "{\"satelliteId\":"+batteryId+", \"Severity\": \"RED LOW\" , \"component\": \"BATT\", \"timestamp\": "+ batteryTS+"}";
				resultEmpty = false;
			}else {				
				result = result + ",{\"satelliteId\":"+batteryId+", \"Severity\": \"RED LOW\" , \"component\": \"BATT\", \"timestamp\": "+ batteryTS+"}";
			}			
		}		
		return result;
	}
	
	/*
	 * Param:  fields, localDateTime
	 * Description: This method will process Thermostate data and check to see if we have an alert to display
	 * if we have an alert then append it to the output result
	 */
	public static String processThermostat(String[] fields,LocalDateTime localDateTime) {
		
		String result="";		
		float rawValue = Float.parseFloat(fields[6]);
		float red_high = Float.parseFloat(fields[2]);
		String component = fields[7];
		if (component.equalsIgnoreCase("TSTAT") && rawValue > red_high) {
			if (thermoCounter == 0) {
				thermoTS = localDateTime.toString();
    			thermoId = fields[1];
			}    			
			thermoCounter++;    				    			
		}
		// If we reach 3 times within 5 mins then append the alert and reset the counter		
		if (thermoCounter == 3) {
			thermoCounter = 0;
			// Append comma if result is not empty
			if (resultEmpty) {
				result = result + "{\"satelliteId\":"+thermoId+", \"Severity\": \"RED HIGH\" , \"component\": \"TSTAT\", \"timestamp\": "+ thermoTS+"}";
				resultEmpty = false;
			}else {
				result = result + ",{\"satelliteId\":"+thermoId+", \"Severity\": \"RED HIGH\" , \"component\": \"TSTAT\", \"timestamp\": "+ thermoTS+"}";				
			}			
		}
		return result;
	}

	/*
	 * Param:  none
	 * Description: This method will process eachline in the file either
	 * BATT or TSTAT it will call either of two methods either processBattary
	 * or processThermostat, also this method will set the 5min interval and reset values
	 * if we reached the 5 min interval so that we can process the next 5 mins data.
	 */
	public static String processFile () throws IOException {
		
		String result= ""; 
		int i = 0;
		boolean batteryInd = false;
    	boolean withinInterval = false;
    	LocalDateTime localDateTime2 = null;
    	
		//place where file is located.
    	String filename = "C:\\input.txt";
		File file = new File(filename); 		
		BufferedReader br = new BufferedReader(new FileReader(file)); 
		System.out.println(filename);
		System.out.println();
		
		while (true)
		{
			String line = br.readLine();
	    	if (line == null) break;
	    	String[] fields = line.split("\\|");	    	
	    	String tsDate = fields[0];	    	
	    	String pattern = "yyyyMMdd HH:mm:ss.SSS";
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
			LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(tsDate));
			
			if (i==0) {
				localDateTime2 = localDateTime.plusMinutes(5);
				withinInterval = true;
			}			
			if (localDateTime.isAfter(localDateTime2)) {
				localDateTime2 = localDateTime.plusMinutes(5);
				battCounter = 0;
				thermoCounter = 0;				
			}    		    		    		    			
    		String component = fields[7];
    		if (component.equalsIgnoreCase("BATT")) {
    			result = result + processBattary(fields, localDateTime);
    		}else if (component.equalsIgnoreCase("TSTAT")) {
    			result = result + processThermostat(fields, localDateTime);
    		}    		
    		i++;
		}
		result = "[" + result + "]";		
		return result;
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Paging Mission Control");
		System.out.println("______________________");
		System.out.println("INPUT:");
		String result = processFile();
		// Display the Output to the console
		System.out.println("OUTPUT:");
		System.out.println(result);			
	}

}
