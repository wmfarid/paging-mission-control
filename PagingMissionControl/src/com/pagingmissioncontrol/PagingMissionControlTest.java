package com.pagingmissioncontrol;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.pagingmissioncontrol.PagingMissionControl;

import org.junit.jupiter.api.Test;

class PagingMissionControlTest {

	@Test
	void testMain() throws IOException {
		System.out.println("Testing testMain()");
		PagingMissionControl.main(null);
	}
	
	@Test
	void testProcessFile() throws IOException {
		System.out.println("Testing testProcessFile()");
		String[] fields = {"20180101 23:01:05.001","1001","101","98","25","20","99.9","TSTAT" };
		PagingMissionControl.processFile();
	}
	
	@Test
	void testProcessBattary() throws IOException {
		System.out.println("Testing testProcessBattary()");
		String[] fields = {"20180101 23:01:05.001","1001","101","98","25","20","99.9","TSTAT" };
		String tsDate = fields[0];	    	
    	String pattern = "yyyyMMdd HH:mm:ss.SSS";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(tsDate));
		PagingMissionControl.processBattary(fields,localDateTime);
	}
	
	@Test
	void testProcessThermostat() throws IOException {
		System.out.println("Testing testProcessThermostat()");
		String[] fields = {"20180101 23:01:05.001","1001","101","98","25","20","99.9","TSTAT" };
		String tsDate = fields[0];	    	
    	String pattern = "yyyyMMdd HH:mm:ss.SSS";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(tsDate));
		PagingMissionControl.processThermostat(fields,localDateTime);
	}

}
